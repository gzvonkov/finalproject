package cryptoTrader.strategy;

import cryptoTrader.app.PerformTrades;
import cryptoTrader.broker.Broker;
import cryptoTrader.utils.AvailableCryptoList;
import cryptoTrader.utils.DataFetcher;

public class StrategyB implements Strategy{
	
	public boolean perform(Broker context) {
		double coin1 = context.getCoin1();
		double coin2 = context.getCoin2();
		if (coin1 > coin2)
			return true;
		else
			return false;
	}
	
	public String getCoin() { //get coin to buy 
		return "Avalanche";    	
	}
	
	public String buyOrSell() { //do we buy or sell according to the strategy
		return "sell";
	}
	
	public int getQuantity() { //get the quantity to buy or sell
		return 10;
	}
		
	public int getPrice() { //get price of coin to buy or sell
		String ID = AvailableCryptoList.getInstance().getCryptoID("Avalanche");
		DataFetcher fetcher = new DataFetcher();
		double price = fetcher.getPriceForCoin(ID, PerformTrades.date() );  
		return (int)price;
	}

}
