package cryptoTrader.strategy;

import cryptoTrader.app.PerformTrades;
import cryptoTrader.broker.Broker;
import cryptoTrader.utils.AvailableCryptoList;
import cryptoTrader.utils.DataFetcher;

public class StrategyA implements Strategy { //Subclass of the Strategy object, with a unique perfrom method
	
	//This is the strategy design pattern this perform method is unqiue to StrategyA objects
	public boolean perform(Broker context) {	
		double coin1 = context.getCoin1(); //Get first coin from Broker
		double coin2 = context.getCoin2(); //Get second coin from Broker
		if (coin1 < coin2) //If first coin smaller then second then successful strategy so return true
			return true;
		else
			return false;
	}
	
	public String getCoin() { //get coin to buy 
		return "Cardano";    	
	}
	
	public String buyOrSell() { //do we buy or sell according to the strategy
		return "buy";
	}
	
	public int getQuantity() { //get the quantity to buy or sell
		return 124;
	}
		
	public int getPrice() { //get price of coin to buy or sell
		String ID = AvailableCryptoList.getInstance().getCryptoID("Cardano");
		DataFetcher fetcher = new DataFetcher();
		double price = fetcher.getPriceForCoin(ID, PerformTrades.date() );  
		return (int)price;
	}
	
}
