package cryptoTrader.strategy;
import cryptoTrader.broker.Broker;

public interface Strategy { //An abstract class for the Strategy. 
	
	//This is the strategy design pattern since different perform methods are applied depending on the context
	public boolean perform(Broker context); //method that exists in all Strategy objects
	
	public String getCoin(); //get coin to buy 
	
	public  String buyOrSell(); //do we buy or sell according to the strategy
	
	public  int getQuantity(); //method that exists in all Strategy objects
	
	public  int getPrice(); //get price of coin to buy

}
