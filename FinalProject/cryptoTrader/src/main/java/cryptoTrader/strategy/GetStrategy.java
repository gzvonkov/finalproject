package cryptoTrader.strategy;

public class GetStrategy { //Class for factory method

	public Strategy getStrategy(String strategy) { //This is the factory method which returns instances of the Strategy object
			
			if ( strategy.equals("Strategy-A") ){ //if Strategy A return a StrategyA object
				return new StrategyA(); 
			}
			else if ( strategy.equals("Strategy-B") ){
				return new StrategyB();
			}
			
			else if ( strategy.equals("Strategy-C") ){
				return new StrategyC();
			}
			
			else if ( strategy.equals("Strategy-D") ){
				return new StrategyD();
			}
			return null;
	}
	
	
}
