package cryptoTrader.gui;

public class Selection {
	
	private String traderName;  //A string for the traders name 
	private String[] coinNames; //An array of string for the coin names
	private String strategyName; //A string for the strategy name
	
	//The constructor for the selection class
	public Selection(String traderName,String[] coinNames,String strategyName) {
		this.traderName = traderName;
		for (String i: coinNames) { //for all coins
			i = i.trim(); //remove the white space before and after the coin
		}
		this.coinNames = coinNames;
		this.strategyName = strategyName;
	}
	
	//Getters for each attribute of the selection
	public String getTraderName(){
		return traderName;
	}
	public String[] getCoinNames(){
		return coinNames; 
	}
	public String getStrategyName() {
		return strategyName;
	}
	
	

}
