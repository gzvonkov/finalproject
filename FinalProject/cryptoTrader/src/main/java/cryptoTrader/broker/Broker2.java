package cryptoTrader.broker;

public class Broker2 extends Broker {
	
	private static Broker2 instance = null;  //create an instance
	
	private Broker2() {}
	
	public static Broker2 getInstance() {
		if (instance == null) {
			instance = new Broker2();
		}
		return instance;		
	}
	
	public String name() {
		return "Broker2";
	}
}
