package cryptoTrader.broker;

public class GetBroker {  //Class for factory method
	

	
	public Broker getBroker(String brokerName) { //This is the factory method which returns instances of the Broker object
		
		if ( brokerName.equals("Broker1") ){	
			return Broker1.getInstance(); //This is the singleton design pattern, since we get the only existent instance of broker 1
		}
		else if (brokerName.equals("Broker2") ){
			return Broker2.getInstance();
		}
		else if (brokerName.equals("Broker3") ){
			return Broker3.getInstance();
		}
		else if (brokerName.equals("Broker4") ){
			return Broker4.getInstance();
		}
		
		return null;	
	}

}
