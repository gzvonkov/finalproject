package cryptoTrader.broker;

public class Broker3 extends Broker {
	
	private static Broker3 instance = null;  //create an instance
	
	private Broker3() {}
	
	public static Broker3 getInstance() {
		if (instance == null) {
			instance = new Broker3();
		}
		return instance;		
	}
	
	public String name() {
		return "Broker3";
	}
}
