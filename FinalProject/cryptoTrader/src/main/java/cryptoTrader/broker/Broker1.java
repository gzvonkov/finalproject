package cryptoTrader.broker;

public class Broker1 extends Broker { //Concrete Broker object is a subclass of the abstract broker class, it uses the singleton design pattern 
	
	private static Broker1 instance = null;  //variable for instance of broker 1
	
	private Broker1() {} //make constructor private, so multiple instances of broker 1 can not be created
	
	public static Broker1 getInstance() {
		if (instance == null) { //If instance does not exist then create it
			instance = new Broker1();
		}
		return instance;	//return the single instance
	}
	
	public String name() {  //return name of the concrete Broker object
		return "Broker1";
	}
	
}
