package cryptoTrader.broker;

public class Broker4 extends Broker{
	
	private static Broker4 instance = null;  //create an instance
	
	private Broker4() {}
	
	public static Broker4 getInstance() {
		if (instance == null) {
			instance = new Broker4();
		}
		return instance;		
	}
	
	public String name() {
		return "Broker4";
	}
	
}
