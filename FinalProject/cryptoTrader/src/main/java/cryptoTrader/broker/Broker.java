package cryptoTrader.broker;

import cryptoTrader.strategy.Strategy;

public abstract class Broker {
	
	double coin1; //coin value for broker
	double coin2; //coin value for broker
	
	private Strategy strategy; //strategy object associated with each broker
	
	int strategyA; //integer to store the number of times strategyA has been successfully performed
	int strategyB;
	int strategyC;
	int strategyD;
	
	public Broker (){ //constructor for the broker object
		strategyA = 0;  //Initialize all strategy integers to 0
		strategyB = 0;
		strategyC = 0;
		strategyD = 0;
	}
	
	public void setCoins(double coin1, double coin2) {  //set the coin values
		this.coin1 = coin1;
		this.coin2 = coin2;
	}
	
	public void setStrategy(Strategy strategy) { //set the strategy object
		this.strategy = strategy;
	}
	
	public double getCoin1() { //getters for the coins
		return coin1;	
	}
	public double getCoin2() {
		return coin2;
	}
	
	public int getStrategyA() {//getters for the strategy integers
		return strategyA;
	}
	public int getStrategyB() {
		return strategyB;
	}
	public int getStrategyC() {
		return strategyC;
	}
	public int getStrategyD() {
		return strategyD;
	}
	
	public void incrementStrategy(String strategy) { //Increment the corresponding strategy integer for the broker object
		if (strategy.equals("Strategy-A")) { //if strategy is A then increment strategy A integer
			this.strategyA++;
		}
		else if (strategy.equals("Strategy-B")) {
			this.strategyB++;
		}
		else if (strategy.equals("Strategy-C")) {
			this.strategyC++;
		}
		else if (strategy.equals("Strategy-D")) {
			this.strategyD++;	
		}
	}
	
	public abstract String name(); //Abstract method to return the name of the concrete Broker object
	
	public boolean execute() {   // This is the strategy design pattern,a broker executes its corresponding strategy
		return strategy.perform(this); //return true if strategy executed successfully
	}
	
}
