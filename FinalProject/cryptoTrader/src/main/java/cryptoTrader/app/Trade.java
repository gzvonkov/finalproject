package cryptoTrader.app;

public class Trade { //Trade class store all information for a line in Trader Action Table
	String result;
	String trader;
	String strategy;
	String coin;
	String action;
	int quantity;
	double price;
	String date;
	
	public Trade(String result,String trader,String strategy,String coin,String action,int quantity,double price,String date ) { //constructor
		this.result = result;
		this.trader = trader;
		this.strategy = strategy;
		this.coin = coin;
		this.action = action;
		this.quantity = quantity;
		this.price = price;
		this.date = date;
	}
	
	public String[] getTrade(){ //get the trade as an array of strings
		String[] trade = {result,trader, strategy, coin, action, String.valueOf(quantity),String.valueOf((int)price),date};
		return trade;
	}
	
	
}
